#include<bits/stdc++.h>
using namespace std;

// Stack implementation
class Stack {
private:
    int maxSize;
    int top;
    int* array;

public:
    Stack(int size) {
        maxSize = size;
        top = -1;
        array = new int[maxSize];
    }

    void push(int value) {
        if (top < maxSize - 1) {
            array[++top] = value;
        }
    }

    int pop() {
        if (top >= 0) {
            return array[top--];
        }
        return -1; // Stack is empty
    }
};

// Queue implementation
class Queue {
private:
    int maxSize;
    int front;
    int rear;
    int* array;

public:
    Queue(int size) {
        maxSize = size;
        front = rear = -1;
        array = new int[maxSize];
    }

    void enqueue(int value) {
        if (rear < maxSize - 1) {
            array[++rear] = value;
            if (front == -1) {
                front = 0;
            }
        }
    }

    int dequeue() {
        if (front <= rear && front != -1) {
            return array[front++];
        }
        return -1; // Queue is empty
    }
};

// Function to check if the stack and queue are the same
bool areStackAndQueueSame(int n, int m, int* stackValues, int* queueValues) {
    Stack stack(n);
    Queue queue(m);

    // Push values into the stack
    for (int i = 0; i < n; ++i) {
        stack.push(stackValues[i]);
    }

    // Enqueue values into the queue
    for (int i = 0; i < m; ++i) {
        queue.enqueue(queueValues[i]);
    }

    // Compare the order of removal
    while (stack.pop() == queue.dequeue()) {
        // Continue removing until the order doesn't match or one of them is empty
        if (stack.pop() == -1 || queue.dequeue() == -1) {
            return true; // Both are empty or have the same order
        }
    }

    return false; // Order doesn't match
}

int main() {
    int n, m;
    cin >> n >> m;

    int stackValues[n];
    int queueValues[m];

    // Input stack values
    for (int i = 0; i < n; ++i) {
        cin >> stackValues[i];
    }

    // Input queue values
    for (int i = 0; i < m; ++i) {
        cin >> queueValues[i];
    }

    // Check if the stack and queue are the same
    if (areStackAndQueueSame(n, m, stackValues, queueValues)) {
        cout << "YES" << endl;
    } else {
        cout << "NO" << endl;
    }

    return 0;
}

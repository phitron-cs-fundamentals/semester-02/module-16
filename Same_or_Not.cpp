#include<bits/stdc++.h>
using namespace std;

int main() {
    int n;
    cin >> n;

    stack<int> s;
    queue<int> q;

    for (int i = 0; i < n; i++) {
        int x;
        cin >> x;
        s.push(x);
    }

    for (int i = 0; i < n; i++) {
        int x;
        cin >> x;
        q.push(x);
    }

    bool flag = true;

    while (!s.empty() && !q.empty()) {
        if (s.top() != q.front()) {
            flag = false;
            break;
        }
        s.pop();
        q.pop();
    }

    if (s.empty() && q.empty()) {
        cout << "YES" << endl;
    } else {
        cout << "NO" << endl;
    }

    return 0;
}

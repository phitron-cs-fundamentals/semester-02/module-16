#include<bits/stdc++.h>
using namespace std;

string isEmptyAfterEliminations(string s) {
    stack<char> st;

    for (char c : s) {
        if (!st.empty() && c == '1') {
            st.pop();
        } else {
            st.push(c);
        }
    }

    return (st.empty() ? "YES" : "NO");
}

int main() {
    int T;
    cin >> T;

    while (T--) {
        string S;
        cin >> S;

        string result = isEmptyAfterEliminations(S);
        cout << result << endl;
    }

    return 0;
}
